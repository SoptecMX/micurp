/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, {Component} from 'react';
import {StyleSheet, View, TextInput} from 'react-native';

export default class Name extends Component {
    focusNextField = (nextField) => {
        this.refs[nextField].focus();
    };
    render() {
        const {nombre, apMat, apPat} = this.props
        return (
            <View style={styles.name}>
                <TextInput ref='1' style={styles.input} value={nombre} returnKeyType={"next"} onChangeText={this.props.onChangeName} underlineColorAndroid="transparent" autoCapitalize="characters" placeholder="Nombre(s)" placeholderTextColor="lightgray" onSubmitEditing={() => this.focusNextField('2')}/>
                <TextInput ref='2' style={styles.input} value={apPat} returnKeyType={"next"} onChangeText={this.props.onChangeApPat} underlineColorAndroid="transparent" autoCapitalize="characters" placeholder="Primer Apellido" placeholderTextColor="lightgray" onSubmitEditing={() => this.focusNextField('3')}/>
                <TextInput ref='3' style={styles.input} value={apMat} onChangeText={this.props.onChangeApMat} underlineColorAndroid="transparent" autoCapitalize="characters" placeholder="Segundo Apellido" placeholderTextColor="lightgray"/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    input: {
        height: 40,
        padding: 10,
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        fontSize: 16,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#48BBEC',
        backgroundColor: 'white'
    },
    name: {
        flex: 1,
        paddingHorizontal: 10,
        justifyContent: 'flex-start'
    }
});
