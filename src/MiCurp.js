/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Button,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  Modal
} from 'react-native';
import SQLite from 'react-native-sqlite-storage';

import Name from './Name'
import Sexo from './Sexo'
import FechaNac from './FechaNac'
import EntFed from './EntFed'
import CurpValid from './CurpValid'

export default class MiCURP extends Component {

  state = {
    nombre: '',
    apPat: '',
    apMat: '',
    sexo: 'S',
    fecha: '',
    estado: 'EF',
    curp: '',
    visible: false
  }

  onChangeName = (nombre) => this.setState({nombre})
  onChangeApPat = (apPat) => this.setState({apPat})
  onChangeApMat = (apMat) => this.setState({apMat})
  onChangeSexo = (sexo) => this.setState({sexo})
  onChangeDate = (fecha) => this.setState({fecha})
  onChangeEstado = (estado) => this.setState({estado})
  setModalVisible = (visible) => this.setState({
    visible: (this.state.visible === true)
      ? false
      : true
  })

  querySQL = (query) => {
    const db = SQLite.openDatabase({
      name: 'mis_curps.db',
      createFromLocation: "~example.db"
    }, this.openCB, this.errorCB);

    db.transaction((tx) => {
      tx.executeSql(query, [], (tx, results) => {
        var len = results.rows.length;
        for (let i = 0; i < len; i++) {
          let row = results.rows.item(i);
          this.setState({saveCurp: this.state.saveCurp.concat(row)});
        }
      });
    });
  }

  curp = () => {
    const {
      nombre,
      apPat,
      apMat,
      fecha,
      sexo,
      estado
    } = this.state
    var miCurp = this.state.curp;
    // Aqui comienzan las primeras cuatro letras del CURP 1-4 posicion
    miCurp = apPat.charAt(0);
    for (var i = 0; i < apPat.length; i++) {
      if (apPat.charAt(i) == 'A' || apPat.charAt(i) == 'E' || apPat.charAt(i) == 'I' || apPat.charAt(i) == 'O' || apPat.charAt(i) == 'U') {
        miCurp = miCurp + apPat.charAt(i)
        i = apPat.length
      }
    }
    miCurp = miCurp + apMat.charAt(0);
    miCurp = miCurp + nombre.charAt(0);
    // Aqui se pone la fecha de nacimiento con el formato correspondiente. 5-10 posicion
    const date = fecha.split("/");
    miCurp = miCurp + date[2].substr(-2) + date[1] + date[0]
    // El sexo el cual viene del archivo Sexo.js 11 posicion
    miCurp = miCurp + sexo;
    // Estado el cual viene de EntFed 12-13 posicion
    miCurp = miCurp + estado;
    //Aqui se pone la primer consonante del nombre y apellidos 14-16 posicion
    for (var i = 1; i < apPat.length; i++) {
      if (apPat.charAt(i) != 'A' && apPat.charAt(i) != 'E' && apPat.charAt(i) != 'I' && apPat.charAt(i) != 'O' && apPat.charAt(i) != 'U') {
        miCurp = miCurp + apPat.charAt(i)
        i = apPat.length
      }
    }
    for (var i = 1; i < apMat.length; i++) {
      if (apMat.charAt(i) != 'A' && apMat.charAt(i) != 'E' && apMat.charAt(i) != 'I' && apMat.charAt(i) != 'O' && apMat.charAt(i) != 'U') {
        miCurp = miCurp + apMat.charAt(i)
        i = apMat.length
      }
    }
    for (var i = 1; i < nombre.length; i++) {
      if (nombre.charAt(i) != 'A' && nombre.charAt(i) != 'E' && nombre.charAt(i) != 'I' && nombre.charAt(i) != 'O' && nombre.charAt(i) != 'U') {
        miCurp = miCurp + nombre.charAt(i)
        i = nombre.length
      }
    }
    // numero verificador 17-18 posicion
    if (date[2] >= 2000) {
      miCurp = miCurp + 'A'
    } else {
      miCurp = miCurp + '0'
    }
    var contador = 18;
    var contador1 = 0;
    var valor = 0;
    var sumatoria = 0;

    while (contador1 <= 15) {

      var pstCom = miCurp.substr(parseInt(contador1), 1);

      switch (pstCom) {
        case '0':
          valor = 0 * contador;
          break
        case '1':
          valor = 1 * contador;
          break;
        case '2':
          valor = 2 * contador;
          break;
        case '3':
          valor = 3 * contador;
          break;
        case '4':
          valor = 4 * contador;
          break;
        case '5':
          valor = 5 * contador;
          break;
        case '6':
          valor = 6 * contador;
          break;
        case '7':
          valor = 7 * contador;
          break;
        case '8':
          valor = 8 * contador;
          break;
        case '9':
          valor = 9 * contador;
          break;
        case 'A':
          valor = 10 * contador;
          break;
        case 'B':
          valor = 11 * contador;
          break;
        case 'C':
          valor = 12 * contador;
          break;
        case 'D':
          valor = 13 * contador;
          break;
        case 'E':
          valor = 14 * contador;
          break;
        case 'F':
          valor = 15 * contador;
          break;
        case 'G':
          valor = 16 * contador;
          break;
        case 'H':
          valor = 17 * contador;
          break;
        case 'I':
          valor = 18 * contador;
          break;
        case 'J':
          valor = 19 * contador;
          break;
        case 'K':
          valor = 20 * contador;
          break;
        case 'L':
          valor = 21 * contador;
          break;
        case 'M':
          valor = 22 * contador;
          break;
        case 'N':
          valor = 23 * contador;
          break;
        case '&':
          valor = 24 * contador;
          break;
        case 'O':
          valor = 25 * contador;
          break;
        case 'P':
          valor = 26 * contador;
          break;
        case 'Q':
          valor = 27 * contador;
          break;
        case 'R':
          valor = 28 * contador;
          break;
        case 'S':
          valor = 29 * contador;
          break;
        case 'T':
          valor = 30 * contador;
          break;
        case 'U':
          valor = 31 * contador;
          break;
        case 'V':
          valor = 32 * contador;
          break;
        case 'W':
          valor = 33 * contador;
          break;
        case 'X':
          valor = 34 * contador;
          break;
        case 'Y':
          valor = 35 * contador;
          break;
        case 'Z':
          valor = 36 * contador;
          break;
      }
      contador = contador - 1;
      contador1 = contador1 + 1;
      sumatoria = sumatoria + valor;
    }
    numVer = sumatoria % 10;
    numVer = Math.abs(10 - numVer);
    if (numVer == 10) {
      numVer = 0;
    }
    miCurp = miCurp + numVer
    this.setState({curp: miCurp})
    this.setModalVisible()
    this.querySQL("INSERT INTO mis_curps (name, apellido_paterno, apellido_materno, sexo, fecha_nacimiento, entidad_federativa, curp, validation) VALUES ('" + nombre + "','" + apPat + "','" + apMat + "','" + sexo + "','" + fecha + "','" + estado + "','" + miCurp + "', '0')")
  }

  limpiar = () => {
    this.setState({
      nombre: 'BRANDOM MARIO',
      apPat: 'MARAÑON',
      apMat: 'LEDEZMA',
      sexo: 'H',
      fecha: '24/10/1993',
      estado: 'PL',
      curp: ''
    })
  }

  render() {
    const {
      nombre,
      apPat,
      apMat,
      sexo,
      fecha,
      estado,
      curp,
      visible
    } = this.state
    return (
      <View style={styles.container}>
        <ScrollView>
          <Name nombre={nombre} apPat={apPat} apMat={apMat} onChangeName={this.onChangeName} onChangeApPat={this.onChangeApPat} onChangeApMat={this.onChangeApMat}/>
          <Sexo onChangeSexo={this.onChangeSexo} sexo={sexo}/>
          <FechaNac onChangeDate={this.onChangeDate} date={fecha}/>
          <EntFed onChangeEstado={this.onChangeEstado} estado={estado}/>
          <View style={styles.btnSend}>
            <Button onPress={this.limpiar} color='#FF9800' title="Limpiar"/>
            <Button onPress={this.curp} color='#FF9800' title="Enviar"/>
          </View>
          <CurpValid querySQL={this.querySQL} modalVisible={this.setModalVisible} visible={visible} curp={curp} nombre={nombre} apPat={apPat} apMat={apMat} sexo={sexo} fecha={fecha} estado={estado}/>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#C8E6C9'
  },
  btnSend: {
    padding: 20,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  }
});
