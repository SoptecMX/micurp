import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Modal,
  Text,
  TouchableOpacity,
  WebView,
  TextInput,
  Clipboard
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default class CurpRes extends Component {
  state = {
    js: false,
    codigo: '',
    message: null,
    clipboard: null
  }
  webview = null;

  onChangeCodigo = (codigo) => this.setState({codigo})

  toggleJs = () => {
    this.setState({
      js: (this.state.js === false)
        ? true
        : false
    })
  }
  toggleJsOn = () => this.setState({
    codigo: '',
    js: true,
    jsCode: "document.querySelector('.js, .svg, .wf-opensans-n3-active, .wf-opensans-n6-active, .wf-opensans-n7-active, .wf-opensans-i3-active, .wf-opensans-i4-active, .wf-opensans-n4-active .wf-active').innerHTML = '<img id=\"capt\" width=\"240\" height=\"60\" name=\"capt\" src=\"/CurpSP/captchaCurp\"> <div style=\"display: none\" class=\"panel-body\"> <form action=\"/CurpSP/gobmx/consultaXDatos.do\" method=\"post\" accept-charset=\"UTF-8\" role=\"form\" id=\"consultaXDatos\" name=\"consultaXdatos\" autocomplete=\"off\" novalidate=\"novalidate\" class=\"bv-form\"><button type=\"submit\" class=\"bv-hidden-submit\" style=\"display: none; width: 0px; height: 0px;\"></button> <input type=\"hidden\" name=\"strtipo\" value=\"A\"> <div class=\"row\"> <div class=\"col-sm-4\"> <div class=\"form-group has-feedback\"> <label class=\"control-label\" for=\"nombre\">Nombre(s)<span class=\"form-text\">*</span>:</label> <input class=\"form-control ns_\" id=\"nombre\" name=\"strNombre\" size=\"20\" maxlength=\"50\" type=\"text\" required=\"\" placeholder=\"Ingrese su nombre\" data-bv-field=\"strNombre\"> <small class=\"help-block\" data-bv-validator=\"notEmpty\" data-bv-for=\"strNombre\" data-bv-result=\"NOT_VALIDATED\" style=\"display: none;\">Este campo es obligatorio</small><small class=\"help-block\" data-bv-validator=\"callback\" data-bv-for=\"strNombre\" data-bv-result=\"NOT_VALIDATED\" style=\"display: none;\">Por favor introduce un valor vÃ¡lido</small><small class=\"help-block\" data-bv-validator=\"stringLength\" data-bv-for=\"strNombre\" data-bv-result=\"NOT_VALIDATED\" style=\"display: none;\">Por favor introduce un valor con una longitud vÃ¡lida</small></div> </div> <div class=\"col-sm-4\"> <div class=\"form-group has-feedback\"> <label class=\"control-label\" for=\"primerApellido\">Primer apellido<span class=\"form-text\">*</span>:</label> <input class=\"form-control ns_\" id=\"primerApellido\" name=\"strPrimerApellido\" size=\"20\" maxlength=\"50\" type=\"text\" required=\"\" placeholder=\"Ingrese su primer apellido\" data-bv-field=\"strPrimerApellido\"> <small class=\"help-block\" data-bv-validator=\"notEmpty\" data-bv-for=\"strPrimerApellido\" data-bv-result=\"NOT_VALIDATED\" style=\"display: none;\">Este campo es obligatorio</small><small class=\"help-block\" data-bv-validator=\"callback\" data-bv-for=\"strPrimerApellido\" data-bv-result=\"NOT_VALIDATED\" style=\"display: none;\">Por favor introduce un valor vÃ¡lido</small><small class=\"help-block\" data-bv-validator=\"stringLength\" data-bv-for=\"strPrimerApellido\" data-bv-result=\"NOT_VALIDATED\" style=\"display: none;\">Por favor introduce un valor con una longitud vÃ¡lida</small></div> </div> <div class=\"col-sm-4\"> <div class=\"form-group has-feedback\"> <label class=\"control-label\" for=\"segundoApellido\">Segundo apellido:</label> <input class=\"form-control ns_\" id=\"segundoApellido\" name=\"strSegundoAplido\" size=\"20\" maxlength=\"50\" type=\"text\" placeholder=\"Ingrese su segundo apellido\" data-bv-field=\"strSegundoAplido\"> <small class=\"help-block\" data-bv-validator=\"callback\" data-bv-for=\"strSegundoAplido\" data-bv-result=\"NOT_VALIDATED\" style=\"display: none;\">Por favor introduce un valor vÃ¡lido</small><small class=\"help-block\" data-bv-validator=\"stringLength\" data-bv-for=\"strSegundoAplido\" data-bv-result=\"NOT_VALIDATED\" style=\"display: none;\">Por favor introduce un valor con una longitud vÃ¡lida</small></div> </div> </div> <div class=\"row\"> <div class=\"col-sm-4\"> <div class=\"form-group has-feedback\"> <label class=\"control-label\">Sexo<span class=\"form-text\">*</span>:</label> <div> <label class=\"radio-inline\"> <input type=\"radio\" name=\"strSexo\" value=\"M\" id=\"sexoM\" class=\"ns_\" data-bv-field=\"strSexo\"> Mujer </label> <label class=\"radio-inline\"> <input type=\"radio\" name=\"strSexo\" value=\"H\" id=\"sexoH\" class=\"ns_\" required=\"\" data-bv-field=\"strSexo\"> Hombre </label> </div> <small class=\"help-block\" data-bv-validator=\"notEmpty\" data-bv-for=\"strSexo\" data-bv-result=\"NOT_VALIDATED\" style=\"display: none;\">Este campo es obligatorio</small></div> </div> <div class=\"col-sm-4\"> <div class=\"form-group datepicker-group has-feedback\"> <label class=\"control-label\" for=\"strFechanacimiento\">Fecha de nacimiento<span class=\"form-text\">*</span>:</label> <input class=\"form-control ns_ hasDatepicker\" id=\"fechaNacimiento\" name=\"strFechanacimiento\" type=\"text\" required=\"\" placeholder=\"Ingrese su fecha de nacimiento\" data-bv-field=\"strFechanacimiento\"> <span class=\"glyphicon glyphicon-calendar\" aria-hidden=\"true\"></span> <small class=\"help-block\" data-bv-validator=\"notEmpty\" data-bv-for=\"strFechanacimiento\" data-bv-result=\"NOT_VALIDATED\" style=\"display: none;\">Este campo es obligatorio</small><small class=\"help-block\" data-bv-validator=\"date\" data-bv-for=\"strFechanacimiento\" data-bv-result=\"NOT_VALIDATED\" style=\"display: none;\">Por favor introduce una fecha vÃ¡lida</small></div> </div> <div class=\"col-sm-4\"> <div class=\"form-group has-feedback\"> <label class=\"control-label\" for=\"entidad\">Entidad Federativa de nacimiento<span class=\"form-text\">*</span>:</label> <select class=\"form-control ns_\" id=\"entidad\" name=\"strEntidadNacimiento\" required=\"\" data-bv-field=\"strEntidadNacimiento\"> <option value=\"\">Seleccione una Entidad Federativa</option> <option value=\"AS\">Aguascalientes</option> <option value=\"BC\">Baja California</option> <option value=\"BS\">Baja California Sur</option> <option value=\"CC\">Campeche</option> <option value=\"CL\">Coahuila de Zaragoza</option> <option value=\"CM\">Colima</option> <option value=\"CS\">Chiapas</option> <option value=\"CH\">Chihuahua</option> <option value=\"DF\">Distrito Federal</option> <option value=\"DG\">Durango</option> <option value=\"GT\">Guanajuato</option> <option value=\"GR\">Guerrero</option> <option value=\"HG\">Hidalgo</option> <option value=\"JC\">Jalisco</option> <option value=\"MC\">Estado de México</option> <option value=\"MN\">Michoacan de Ocampo</option> <option value=\"MS\">Morelos</option> <option value=\"NT\">Nayarit</option> <option value=\"NL\">Nuevo León</option> <option value=\"OC\">Oaxaca</option> <option value=\"PL\">Puebla</option> <option value=\"QT\">Queretaro de Arteaga</option> <option value=\"QR\">Quintana Roo</option> <option value=\"SP\">San Luis Potosi</option> <option value=\"SL\">Sinaloa</option> <option value=\"SR\">Sonora</option> <option value=\"TC\">Tabasco</option> <option value=\"TS\">Tamaulipas</option> <option value=\"TL\">Tlaxcala</option> <option value=\"VZ\">Veracruz</option> <option value=\"YN\">Yucatan</option> <option value=\"ZS\">Zacatecas</option> <option value=\"NE\">Nacido en el Extranjeros</option> </select> <small class=\"help-block\" data-bv-validator=\"notEmpty\" data-bv-for=\"strEntidadNacimiento\" data-bv-result=\"NOT_VALIDATED\" style=\"display: none;\">Este campo es obligatorio</small></div> </div> </div> <div class=\"row\"> <div class=\"col-sm-4\"> <div class=\"form-group has-feedback\"> <label class=\"control-label\" for=\"codigo\">Código de verificación<span class=\"form-text\">*</span>:</label> <input class=\"form-control ns_\" name=\"codigo\" id=\"captcha2\" maxlength=\"5\" type=\"text\" required=\"\" placeholder=\"Ingresa el código mostrado en la imagen\" data-bv-field=\"codigo\"> <small class=\"help-block\" data-bv-validator=\"notEmpty\" data-bv-for=\"codigo\" data-bv-result=\"NOT_VALIDATED\" style=\"display: none;\">Este campo es obligatorio</small><small class=\"help-block\" data-bv-validator=\"stringLength\" data-bv-for=\"codigo\" data-bv-result=\"NOT_VALIDATED\" style=\"display: none;\">El código de verificación debe ser de 5 caracteres de longitud</small><small class=\"help-block\" data-bv-validator=\"regexp\" data-bv-for=\"codigo\" data-bv-result=\"NOT_VALIDATED\" style=\"display: none;\">El código de verificación solo puede contener números y letras</small></div> </div> </div> <div class=\"row\"> <div class=\"col-md-8 col-md-offset-4\"> <hr> </div> </div> <div class=\"clearfix\"> <div class=\"pull-left text-muted text-vertical-align-button\">* Campos obligatorios<br>* Formato para fechas: dd/mm/aaaa</div> <div class=\"pull-right\"> <button class=\"btn btn-default\" type=\"reset\" name=\"resetForm\">Descartar</button> <button class=\"btn btn-primary\" type=\"submit\" id=\"con_datos\"><span class=\"glyphicon glyphicon-search\"></span> Buscar</button> </div> </div> </form> </div>'; document.getElementsByTagName('body')[0].style.margin = '0px'; document.consultaXdatos.nombre.value = '" + this.props.nombre + "'; document.consultaXdatos.primerApellido.value = '" + this.props.apPat + "';document.consultaXdatos.segundoApellido.value = '" + this.props.apMat + "'; document.consultaXdatos.sexo" + this.props.sexo + ".checked = 'true'; document.consultaXdatos.fechaNacimiento.value = '" + this.props.fecha + "'; document.consultaXdatos.entidad.value = '" + this.props.estado + "';"
  })

  onMessage = (e) => this.setState({message: e.nativeEvent.data})

  injectJS = (jsCode) => {
    this.setState({
      jsCode
    }, generateJsCodes = () => {
      if (this.webview) {
        this.webview.injectJavaScript(this.state.jsCode);
      }
    })
  }

  jscript = () => {
    this.injectJS("document.consultaXdatos.codigo.value = '" + this.state.codigo + "'; document.consultaXdatos.con_datos.click();")
    this.props.onRefresh()
  }

  setClipboardContent = async() => {
    Clipboard.setString(this.props.curp);
    try {
      const clipboard = await Clipboard.getString();
      this.setState({clipboard});
    } catch (e) {
      this.setState({clipboard: e.message});
    }
  };

  toggleCopied = () => {
    if (this.props.curp === this.state.clipboard) {
      return styles.copied
    } else {
      return styles.copy
    }
  }

  render() {
    const {modalVisible, visible, curp, sexo} = this.props
    const {message, js, jsCode, codigo} = this.state
    if (curp === message) {
      this.props.querySQL('UPDATE mis_curps SET Validation = 1 WHERE Curp = "' + message + '"')
    }
    return (
      <Modal animationType={"fade"} transparent={true} visible={visible} onShow={this.toggleJsOn} onRequestClose={modalVisible}>
        <View style={[styles.container, styles.modalBackgroundStyle]}>
          <View style={[styles.innerContainer, styles.innerContainerTransparentStyle]}>
            <View style={styles.textView}>
              <Text style={styles.textCurp}>{curp}</Text>
              <TouchableOpacity style={this.toggleCopied()} onPress={this.setClipboardContent}>
                <Icon name="md-copy" size={30} color="white"/>
              </TouchableOpacity>
            </View>
            <View style={styles.webView}>
              <WebView ref={webview => {
                this.webview = webview;
              }} source={{
                uri: 'https://consultas.curp.gob.mx/CurpSP/gobmx/inicio.jsp'
              }} onMessage={this.onMessage} startInLoadingState={true} onLoadEnd={() => this.injectJS("setTimeout(function(){ postMessage(document.getElementById('strCurp').innerText, '*')}, 100);")} javaScriptEnabled={js} injectedJavaScript={jsCode}/>
            </View>
            <TextInput style={styles.input} underlineColorAndroid="darkgray" value={codigo} onChangeText={this.onChangeCodigo} autoCapitalize="characters" placeholder="Codigo de Verificación" placeholderTextColor="lightgray"/>
            <View style={styles.buttons}>
              <TouchableOpacity onPress={modalVisible}>
                <View style={styles.modalButton}>
                  <Text style={styles.buttonText}>Close</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.jscript}>
                <View style={styles.modalButton}>
                  <Text style={styles.buttonText}>Validar</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 20
  },
  innerContainer: {
    borderRadius: 10,
    alignItems: 'center'
  },
  buttonText: {
    fontSize: 18,
    margin: 5,
    textAlign: 'center'
  },
  modalButton: {
    marginTop: 10
  },
  modalBackgroundStyle: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)'
  },
  innerContainerTransparentStyle: {
    backgroundColor: '#fff',
    padding: 20
  },
  buttons: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    justifyContent: 'space-around'
  },
  webView: {
    height: 64,
    width: 240
  },
  input: {
    textAlign: 'center',
    width: 165
  },
  textStyle: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 13,
    paddingHorizontal: 10
  },
  textCurp: {
    fontSize: 13,
    paddingHorizontal: 10
  },
  textView: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
    marginBottom: 10,
    borderWidth: 1,
    borderRadius: 6,
    borderColor: '#48BBEC',
    backgroundColor: 'white'
  },
  copy: {
    backgroundColor: '#FF9800',
    height: '100%',
    width: 40,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    alignItems: 'center',
    justifyContent: 'center'
  },
  copied: {
    backgroundColor: 'green',
    height: '100%',
    width: 40,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
