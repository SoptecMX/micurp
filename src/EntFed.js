/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, {Component} from 'react';
import {StyleSheet, View, Picker} from 'react-native';

export default class entFed extends Component {
    estados = [
        {
            shortEdo: 'EF',
            Edo: 'Entidad Federativa'
        }, {
            shortEdo: 'AS',
            Edo: 'AGUASCALIENTES'
        }, {
            shortEdo: 'BC',
            Edo: 'BAJA CALIFORNIA'
        }, {
            shortEdo: 'BS',
            Edo: 'BAJA CALIFORNIA SUR'
        }, {
            shortEdo: 'CC',
            Edo: 'CAMPECHE'
        }, {
            shortEdo: 'CL',
            Edo: 'COAHUILA DE ZARAGOZA'
        }, {
            shortEdo: 'CM',
            Edo: 'COLIMA'
        }, {
            shortEdo: 'CS',
            Edo: 'CHIAPAS'
        }, {
            shortEdo: 'DF',
            Edo: 'DISTRITO FEDERAL'
        }, {
            shortEdo: 'DG',
            Edo: 'DURANGO'
        }, {
            shortEdo: 'MC',
            Edo: 'ESTADO DE MEXICO'
        }, {
            shortEdo: 'GT',
            Edo: 'GUANAJUATO'
        }, {
            shortEdo: 'GR',
            Edo: 'GUERRERO'
        }, {
            shortEdo: 'HG',
            Edo: 'HIDALGO'
        }, {
            shortEdo: 'JC',
            Edo: 'JALISCO'
        }, {
            shortEdo: 'MN',
            Edo: 'MICHOACAN DE OCAMPO'
        }, {
            shortEdo: 'MS',
            Edo: 'MORELOS'
        }, {
            shortEdo: 'NT',
            Edo: 'NAYARIT'
        }, {
            shortEdo: 'NL',
            Edo: 'NUEVO LEON'
        }, {
            shortEdo: 'OC',
            Edo: 'OAXACA'
        }, {
            shortEdo: 'PL',
            Edo: 'PUEBLA'
        }, {
            shortEdo: 'QT',
            Edo: 'QUERETARO DE ARTEAGA'
        }, {
            shortEdo: 'QR',
            Edo: 'QUINTANA ROO'
        }, {
            shortEdo: 'SP',
            Edo: 'SAN LUIS POTOSI'
        }, {
            shortEdo: 'SL',
            Edo: 'SINALOA'
        }, {
            shortEdo: 'SR',
            Edo: 'SONORA'
        }, {
            shortEdo: 'TC',
            Edo: 'TABASCO'
        }, {
            shortEdo: 'TS',
            Edo: 'TAMAULIPAS'
        }, {
            shortEdo: 'TL',
            Edo: 'TLAXCALA'
        }, {
            shortEdo: 'VZ',
            Edo: 'VERACRUZ'
        }, {
            shortEdo: 'YN',
            Edo: 'YUCATAN'
        }, {
            shortEdo: 'ZS',
            Edo: 'ZACATECAS'
        }, {
            shortEdo: 'NE',
            Edo: 'NACIDO EN EL EXTRANJERO'
        }
    ]

    stado = () => {
        const edos = this.estados.map((data) => {
            const Item = Picker.Item;
            return <Item value={data.shortEdo} label={data.Edo}/>
        })
        return edos;
    }

    entidadFed = () => {
        if (this.props.estado === 'EF') {
            return styles.pickerOff
        } else {
            return styles.pickerOn
        }
    }

    render() {
        return (
            <View style={styles.entFed}>
                <View style={styles.viewEntfed}>
                    <Picker style={this.entidadFed()} selectedValue={this.props.estado} onValueChange={this.props.onChangeEstado}>
                        {this.stado()}
                    </Picker>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    entFed: {
        flex: 1,
        paddingHorizontal: 10,
        justifyContent: 'flex-start'
    },
    viewEntfed: {
        height: 40,
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#48BBEC',
        backgroundColor: 'white'
    },
    pickerOff: {
        color: 'lightgray',
        height: 40
    },
    pickerOn: {
        height: 40
    }
});
