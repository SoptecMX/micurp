/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, {Component} from 'react';
import {StyleSheet, View, Picker} from 'react-native';

export default class sexo extends Component {
    toggle = () => {
        const {sexo} = this.props;
        const Item = Picker.Item;
        if (sexo === 'S') {
            return (
                <Picker style={styles.pickSexoDef} selectedValue={sexo} onValueChange={this.props.onChangeSexo} mode="dropdown">
                    <Item label="Sexo" value="S"/>
                    <Item label="MASCULINO" value="H"/>
                    <Item label="FEMENINO" value="M"/>
                </Picker>
            )
        } else {
            return (
                <Picker style={styles.pickSexo} selectedValue={sexo} onValueChange={this.props.onChangeSexo} mode="dropdown">
                    <Item label="MASCULINO" value="H"/>
                    <Item label="FEMENINO" value="M"/>
                </Picker>
            )
        }
    }

    render() {
        return (
            <View style={styles.viewSexo}>
                <View style={styles.sexo}>
                    {this.toggle()}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    pickSexoDef: {
        color: "lightgray",
        height: 40
    },
    pickSexo: {
        height: 40
    },
    sexo: {
        height: 40,
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#48BBEC',
        backgroundColor: 'white'
    },
    viewSexo: {
        flex: 1,
        paddingHorizontal: 10,
        justifyContent: 'flex-start'
    }
});
