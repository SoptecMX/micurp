import React, {Component} from 'react';
import {
  ScrollView,
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  RefreshControl
} from 'react-native';

import CurpValid from './CurpValid'
import MisCurpsBox from './MisCurpsBox'

export default class CurpList extends Component {
  state = {
    visible: false,
    curp: '',
    refreshing: false
  }

  setModalVisible = (visible) => this.setState({
    visible: (this.state.visible === true)
      ? false
      : true
  })

  itemSelected = (index) => {
    const {curps} = this.props
    this.setState({
      nombre: curps[index].name,
      apPat: curps[index].apellido_paterno,
      apMat: curps[index].apellido_materno,
      sexo: curps[index].sexo,
      fecha: curps[index].fecha_nacimiento,
      estado: curps[index].entidad_federativa,
      curp: curps[index].curp
    })
    this.setModalVisible()
  }

  onRefresh = () => {
    this.setState({
      refreshing: true
    }, fetchData = () => {
      this.props.querySQL('SELECT * FROM mis_curps')
      this.setState({refreshing: false});
    })
  }

  render() {
    const {curps} = this.props
    const {
      nombre,
      apPat,
      apMat,
      sexo,
      fecha,
      estado,
      visible,
      curp
    } = this.state
    return (
      <ScrollView style={styles.scroll} refreshControl={< RefreshControl refreshing = {
        this.state.refreshing
      }
      onRefresh = {
        this.onRefresh
      } />}>
        <MisCurpsBox curps={curps} itemSelected={this.itemSelected}/>
        <CurpValid onRefresh={this.onRefresh} querySQL={this.props.querySQL} modalVisible={this.setModalVisible} visible={visible} curp={curp} nombre={nombre} apPat={apPat} apMat={apMat} sexo={sexo} fecha={fecha} estado={estado}/>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  scroll: {
    width: '100%'
  },
  containerView: {
    paddingHorizontal: 10
  }
});
