/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';

import DatePicker from 'react-native-datepicker'

export default class fechaNac extends Component {

  render() {
    return (
      <View style={styles.fechaNac}>
        <View style={styles.viewFecha}>
          <DatePicker style={styles.fecha} customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0
            },
            dateInput: {
              flex: 1,
              height: 40,
              borderWidth: 1,
              marginLeft: 36,
              bottom: 2,
              borderColor: 'transparent',
              alignItems: 'flex-start',
              justifyContent: 'center'
            },
            placeholderText: {
              color: 'lightgray',
              fontSize: 16
            },
            dateText: {
              color: '#333',
              fontSize: 16
            }
          }} date={this.props.date} androidMode='spinner' mode="date" placeholder="Fecha de Nacimiento" format="DD/MM/YYYY" confirmBtnText="Confirmar" cancelBtnText="Cancelar" onDateChange={this.props.onChangeDate}/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  fechaNac: {
    flex: 1,
    paddingHorizontal: 10,
    justifyContent: 'flex-start'
  },
  viewFecha: {
    height: 40,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#48BBEC',
    backgroundColor: 'white'
  },
  fecha: {
    marginLeft: 0,
    width: '100%'
  }
});
