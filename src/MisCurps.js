/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {AppRegistry, StyleSheet, Text, View} from 'react-native';

import SQLite from 'react-native-sqlite-storage';
import CurpValid from './CurpValid';
import CurpList from './CurpList'

export default class MisCurps extends Component {
  state = {
    saveCurp: []
  }

  querySQL = (query) => {
    this.setState({saveCurp: []})
    const db = SQLite.openDatabase({
      name: 'mis_curps.db',
      createFromLocation: "~example.db"
    }, this.openCB, this.errorCB);

    db.transaction((tx) => {
      tx.executeSql(query, [], (tx, results) => {
        var len = results.rows.length;
        for (let i = 0; i < len; i++) {
          let row = results.rows.item(i);
          this.setState({saveCurp: this.state.saveCurp.concat(row)});
        }
      });
    });
  }

  errorCB = (err) => {
    console.log("SQL Error: " + err);
  }

  openCB = () => {
    console.log("Database OPENED");
  }
  componentWillMount() {
    this.querySQL('SELECT * FROM mis_curps')
  }

  render() {
    const {saveCurp} = this.state
    return (
      <View style={styles.container}>
        <CurpList querySQL={this.querySQL} curps={saveCurp}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  }
});
