import React, {Component} from 'react';
import {ScrollView, TouchableOpacity, Text, View, StyleSheet} from 'react-native';

export default class MisCurpsBox extends Component {

  misCurps = () => {
    return this.props.curps.map((data, index) => {
      return (
        <View style={styles.curpBox}>
          <TouchableOpacity onPress={() => this.props.itemSelected(index)}>
            <Text>{data.name} {data.apellido_paterno}
              {data.apellido_materno}</Text>
            <Text>{data.curp}</Text>
            <Text>{data.validation}</Text>
          </TouchableOpacity>
        </View>
      )
    })
  }

  render() {
    return (
      <View style={styles.containerView}>
        {this.misCurps()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerView: {
    paddingHorizontal: 10
  },
  curpBox: {
    paddingVertical: 5
  }
});
