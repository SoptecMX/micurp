import React, {Component} from 'react';
import {AppRegistry, TouchableOpacity, View} from 'react-native';
import {StackNavigator, DrawerNavigator} from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

import MiCurp from './src/MiCurp'
import MisCurps from './src/MisCurps'

const HomeScreen = StackNavigator({
    Home: {
        screen: MiCurp,
        navigationOptions: {
            header: ({navigate}) => ({
                title: 'MiCURP', left: <TouchableOpacity onPress={() => navigate('DrawerOpen')}>
                    <Icon name="md-menu" size={30} color="white"/>
                </TouchableOpacity>,
                right: <View></View>,
                titleStyle: {
                    color: 'white',
                    alignSelf: 'center'
                },
                style: {
                    backgroundColor: '#4CAF50',
                    paddingHorizontal: 10
                }
            })
        }
    }
});

const MisCurpsScreen = StackNavigator({
    MisCurps: {
        screen: MisCurps,
        navigationOptions: {
            header: ({navigate}) => ({
                title: 'Mis CURPS', left: <TouchableOpacity onPress={() => navigate('DrawerOpen')}>
                    <Icon name="md-menu" size={30} color="white"/>
                </TouchableOpacity>,
                right: <View></View>,
                titleStyle: {
                    color: 'white',
                    alignSelf: 'center'
                },
                style: {
                    backgroundColor: '#4CAF50',
                    paddingHorizontal: 10
                }
            })
        }
    }
})

const DrawMenu = DrawerNavigator({
    Home: {
        screen: HomeScreen,
        navigationOptions: {
            drawer: () => ({label: 'Curp', icon: (<Icon name="ios-home" size={25}/>)})
        }
    },
    CurpSave: {
        screen: MisCurpsScreen,
        navigationOptions: {
            drawer: () => ({label: 'Mis Curps', icon: (<Icon name="ios-folder" size={25}/>)})
        }
    }
});

AppRegistry.registerComponent('MiCURP', () => DrawMenu);;
